Laboratory 9 requirements

Authors Joanna Bieganowska

Tasks:
1. Views consisting the use of JSF technology are selectRandomShow.xhtml, showPersonalizedList.xhtml, showRandomShow.xhtml, userMood.xhtml
   The above views have been implemented using latest i.e. JSF 2.3 version.
	

2. Bean validation implementation can be found in the above mentioned views as well as in Backing beans RandomShowBackingBean and UserMoodBackingBean.
   I have implemented 3 types of validations mentioned as below:
   1. Declarative Validator i.e Inline page validation eg. validation in the xhtml page tags like "required, length, etc.".
   2. Imperative validation:
   	  i. Using bean method for validation, eg: in UserMoodBackingBean.java class the method validateUserMood() is used for validation the user mood and 
   	  for incorrect inputs it returns appropriate error message which is displayed in the html page.
   	  ii. Using Custom JSF Validator, eg: in UserMoodBackingBean.java class the method validate() is overridden from the implementation Validator interface.
   	  	  This custom validator validates the type of show entered by the user and in case of wrong input it throws appropriate exception which is sent to 
   	  	  the html page and displayed to the user.
	

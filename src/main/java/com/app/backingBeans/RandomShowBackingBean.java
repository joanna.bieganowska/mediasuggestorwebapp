package com.app.backingBeans;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.app.beans.Show;
import com.app.service.ShowRenderingService;

import lombok.Getter;
import lombok.Setter;

/**
 * Class with bean validation.
 * 
 * @author Joanna Bieganowska
 *
 */
@Named
@SessionScoped
public class RandomShowBackingBean implements Serializable {

	private static final long serialVersionUID = 2158217747652495823L;

	@Getter
	@Setter
	private String[] showType;

	@Getter
	private List<Show> randomShowList;

	private ShowRenderingService showRenderingService = null;

	/**
	 * Method in which random show is selected and response is sent to showRandomShow.xhtml
	 * @return string URL
	 */
	public String getRandomShow() {
		showRenderingService = new ShowRenderingService();
		randomShowList = showRenderingService.getOneRandomMovieVideoTvShow(showType);
		return "/showRandomShow?faces-redirect=true";
	}
}

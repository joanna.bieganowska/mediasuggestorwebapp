package com.app.backingBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

import com.app.beans.Show;
import com.app.service.ShowRenderingService;

import lombok.Getter;
import lombok.Setter;

/**
 * Class that uses bean validation.
 * 
 * @author Joanna Bieganowska
 *
 */
@Named
@SessionScoped
@FacesValidator("typeOfShowValidator")
public class UserMoodBackingBean implements Serializable, Validator {

	private static final long serialVersionUID = -5780468381739131717L;

	@Getter
	@Setter
	private String userMood;

	@Getter
	@Setter
	private String typeOfShow;

	@Getter
	private List<Show> output;

	@Getter
	private List<Show> showsBasedOnMood;

	@Getter
	private List<String> typeOfShows;

	private ShowRenderingService showRenderingService = null;

	/**
	 * Non-argument constructor
	 */
	public UserMoodBackingBean() {
		typeOfShows = new ArrayList<String>();
		typeOfShows.add("Movie");
		typeOfShows.add("Tv Show");
		typeOfShows.add("Video");
	}

	/**
	 * Method for getting personalized show based on mood and redirecting it to
	 * showPersonalizedList.xhtml
	 * 
	 * @return string URL
	 */
	public String showBasedOnMood() {
		showRenderingService = new ShowRenderingService();
		showsBasedOnMood = showRenderingService.showBasedOnMoodService(userMood, typeOfShow);
		return "/showPersonalizedList?faces-redirect=true";
	}

	/**
	 * Validation method for user mood
	 * 
	 * @param context FacesContext
	 * @param comp    UIComponent
	 * @param mood    Object string
	 */
	public void validateUserMood(FacesContext context, UIComponent comp, Object mood) {
		String userMood = (String) mood;
		if (!userMood.equalsIgnoreCase("Happy") && !userMood.equalsIgnoreCase("Sad")
				&& !userMood.equalsIgnoreCase("Bored")) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage(" The User Mood can be either Happy, Bored or Sad");
			context.addMessage(comp.getClientId(context), message);
		}
	}

	/**
	 * Method for validating type of show
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object typeOfShow) throws ValidatorException {

		String showType = (String) typeOfShow;
		if (!showType.equalsIgnoreCase("Movie") && !showType.equalsIgnoreCase("TvShow")
				&& !showType.equalsIgnoreCase("Video")) {
			FacesMessage msg = new FacesMessage(" The type of show should be either Movie, TvShow or Video");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);

			throw new ValidatorException(msg);
		}
	}

}

package com.app.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Proxy;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Link is a class that consists of necessary attributes creating URL. Whenever
 * clone of Show object is created. then a clone of Link object is also created
 * in respect of deep cloning.
 * 
 * @author Joanna Bieganowska
 *
 */
@Entity
@Proxy(lazy = false)
@NoArgsConstructor
public class Link implements Cloneable, Serializable {
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -2789021713733833747L;

	/**
	 * ID of the link
	 */
	@Id
	@Getter
	@Setter
	private int lId;

	/**
	 * Link is mapped with show by many to one relationship
	 */
	@ManyToOne
	@Getter
	@Setter
	private Show show;

	@Getter
	@Setter
	protected String website;

	/**
	 * Domain can be for example com, pl.
	 */
	@Getter
	@Setter
	protected String domain;

	/**
	 * Extension is "tail" of URL - text followed by slash.
	 */
	@Getter
	@Setter
	protected String extension;

	/**
	 * Secure protocol which determines whether website is https or http. In case of
	 * True - https (secured) In case of False - http (not secured)
	 */
	@Getter
	@Setter
	protected boolean secureProtocol;

	/**
	 * Validity of the link. True for working link False for broken link (not
	 * working)
	 */
	@Getter
	@Setter
	protected boolean validity;

	/**
	 * Parameterised constructor for creating Link object.
	 * 
	 * @param lId            ID of the link
	 * @param website        name of a website for example Netflix
	 * @param domain         can be for example com, pl.
	 * @param extension      "tail" of URL - text followed by slash.
	 * @param secureProtocol Secure protocol which determines whether website is
	 *                       https or http. In case of True - https (secured). In
	 *                       case of False - http (not secured)
	 * @param validity       Validity of the link. True for working link. False for
	 *                       broken link (not working)
	 */
	public Link(Integer lId, String website, String domain, String extension, boolean secureProtocol,
			boolean validity) {
		super();
		this.lId = lId;
		this.website = website;
		this.domain = domain;
		this.extension = extension;
		this.secureProtocol = secureProtocol;
		this.validity = validity;
		// this.show = show;
	}

	/**
	 * Method in which validity is changed to opposite for example from true to
	 * false and vice versa
	 */
	public void swapValidity() {
		this.validity = !this.validity;
	}

	/**
	 * toString method to display Link object - object is represented as a full link
	 * to the show.
	 */
	@Override
	public String toString() {
		if (this.secureProtocol == true)
			return "https://" + website + "." + domain + "/" + extension;
		else
			return "http://" + website + "." + domain + "/" + extension;
	}

	/**
	 * Clone method from Cloneable interface.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}

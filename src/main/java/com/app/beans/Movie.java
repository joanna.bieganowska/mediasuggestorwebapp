package com.app.beans;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.Entity;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Movie is a class that extends from Show, that is - it has attributes from
 * Show as well as its own.
 * 
 * @author Joanna Bieganowska
 */

@Entity
@NoArgsConstructor
public class Movie extends Show implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 547341714436838549L;

	/**
	 * ID of a movie.
	 */
	@Getter
	@Setter
	private Integer movieId;

	/**
	 * movieLength is a value in minutes
	 */
	@Getter
	@Setter
	private Integer movieLength;

	/**
	 * productionCompany is a company that produced the movie, for example: Netflix
	 */
	@Getter
	@Setter
	private String productionCompany;

	/**
	 * movieRating is rating in scale 0-10
	 */
	@Getter
	@Setter
	private Double movieRating;

	/**
	 * suggestionForNomination is application's suggestion whether Movie should be
	 * nominated. For future implementation.
	 */
	@Transient
	@Getter
	@Setter
	private Boolean suggestionForNomination;
	/**
	 * movieList is a list of movies, needed for data generation.
	 */
	@Transient
	protected List<Movie> movieList = new ArrayList<Movie>();

	/**
	 * movies is list of movies used by threads
	 */
	@Transient
	public List<Movie> movies = new ArrayList<Movie>();

	/**
	 * Parameterised constructor for creating Movie object
	 * 
	 * @param showId            ID of the show
	 * @param typeOfShow        type of show. Possible values: TV show, movie,
	 *                          video.
	 * @param genre             Genre of show. Possible values are for example:
	 *                          comedy, drama etc.
	 * @param showTitle         name of show.
	 * @param realiseDate       realiseDate is date of realise of show. In case of
	 *                          video it is a date of video upload on Youtube. In
	 *                          case of movies it is a date of first global realise.
	 * @param movieId           ID of movie
	 * @param movieLength       length of movie in minutes.
	 * @param productionCompany company that produced the movie, for example:
	 *                          Netflix
	 * @param movieRating       rating in scale 0-10
	 */
	public Movie(Integer showId, String typeOfShow, String genre, String showTitle, String realiseDate, /*
																										 * Link
																										 * showLink,
																										 */
			Integer movieId, Integer movieLength, String productionCompany, Double movieRating) {
		super(showId, typeOfShow, genre, showTitle, realiseDate/* , showLink */);
		this.movieId = movieId;
		this.movieLength = movieLength;
		this.productionCompany = productionCompany;
		this.movieRating = movieRating;
	}


	/**
	 * toString method to display Movie object.
	 */
	@Override
	public String toString() {
		return "Movie [movieId=" + movieId + ", showTitle" + super.getShowTitle() + ", typeOfShow="
				+ super.getTypeOfShow() + ", genre=" + super.getGenre() + ", showTitle=" + super.getShowTitle()
				+ ", realiseDate=" + super.getRealiseDate() + ", showLink=" + super.getShowLink() + ", movieLength="
				+ movieLength + ", productionCompany=" + productionCompany + ", movieRating=" + movieRating
				+ ", suggestionForNomination=" + suggestionForNomination + "]";
	}

	/**
	 * Overridden method implemented from runnable interface. It paralelly generated
	 * data with the use of threads.
	 */
	@Override
	public void run() {
		System.out.println("Generating movies...");
		Random random = new Random();

		/**
		 * Lists with possible values
		 */
		List<String> genre = Arrays.asList("comedy", "action", "drama", "horror", "thriller", "musical", "scifi");
		List<String> websites = Arrays.asList("Netflix", "YouTube", "cda", "Amazon", "CoolMovies", "MoviesAllTime");
		List<String> domains = Arrays.asList("com", "pl", "fr", "in", "uk", "org");
		List<String> producers = Arrays.asList("Netflix", "Warner", "Disney", "Pixar", "Paramount", "BestProducer");

		/**
		 * Creating passed number of movie objects.
		 */
		for (int i = 1; i <= numberOfObjects; i++) {

			/**
			 * Random index selection of above lists
			 */
			Integer genreInt = random.nextInt(6);
			Integer websiteInt = random.nextInt(6);
			Integer domainInt = random.nextInt(6);
			Integer procuderInt = random.nextInt(6);

			/**
			 * Generating random string
			 */
			int leftLimit = 65; // from A
			int rightLimit = 122; // to z
			int stringLength = 7;
			StringBuilder buffer = new StringBuilder(stringLength);
			for (int j = 0; j < stringLength; j++) {
				int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
				buffer.append((char) randomLimitedInt);
			}
			String generatedString = buffer.toString();

			/**
			 * Generating random date
			 */
			LocalDate from = LocalDate.of(2010, 1, 1);
			LocalDate to = LocalDate.of(2020, 4, 4);
			long days = from.until(to, ChronoUnit.DAYS);
			long randomDays = ThreadLocalRandom.current().nextLong(days + 1);
			LocalDate randomDate = from.plusDays(randomDays);
			String ranDate = randomDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			/**
			 * Generating more random values for movie attributes
			 */
			Boolean secure = random.nextBoolean();
			Boolean valid = random.nextBoolean();
			Integer randomLength = random.nextInt(200);
			Double ranRating = Math.round((0 + (random.nextDouble() * (10))) * 100.0) / 100.0;

			Link link = new Link(i, websites.get(websiteInt), domains.get(domainInt), generatedString, secure, valid);
			/**
			 * Creating Movie object
			 */
			Movie movie = new Movie(i, "movie", genre.get(genreInt), generatedString, ranDate, i, randomLength,
					producers.get(procuderInt), ranRating);

			movie.getShowLink().add(link);
			link.setShow(movie);

			updateList(movie);
		}
		System.out.println("Movies data creation done.");
	}

	/**
	 * updateList is used as critical section. Here, multiple threads may want to
	 * access the same data. The problem solved with synchronized block.
	 */
	private void updateList(Movie movie) {
		synchronized (movies) {
			movies.add(movie);
		}
		synchronized (movieList) {
			movieList.add(movie);
		}
	}

}

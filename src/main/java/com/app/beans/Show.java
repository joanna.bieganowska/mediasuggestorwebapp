package com.app.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor
public abstract class Show implements Cloneable, Serializable {

	/**
	 * Serialized version ID (generated by program)
	 */
	private static final long serialVersionUID = -4453034623599755125L;

	@Id
	@Getter
	@Setter
	public Integer showId;

	/**
	 * typeOfShow is type of show. Possible values: TV show, movie, video.
	 */
	@Getter
	@Setter
	private String typeOfShow;

	/**
	 * Genre of show. Possible values are for example: comedy, drama etc.
	 */
	@Getter
	@Setter
	private String genre;

	/**
	 * showTitle is name of show.
	 */
	@Getter
	@Setter
	private String showTitle;

	/**
	 * realiseDate is date of realise of show. In case of video it is a date of
	 * video upload on Youtube. In case of movies it is a date of first global
	 * realise.
	 */
	@Getter
	@Setter
	private String realiseDate;

	/**
	 * showLink is a list of links of the show. It can navigate to Youtube, Netflix
	 * or other websites.
	 */
	@OneToMany(mappedBy = "show", fetch = FetchType.EAGER)
	@Getter
	@Setter
	private List<Link> showLink = new ArrayList<Link>();

	/**
	 * Number of objects that each class inheriting from Show class will generate.
	 */
	@Transient
	public static int numberOfObjects = 100; // TO TEST

	/**
	 * Parameterised constructor for creating Show object
	 * 
	 * @param showId      ID of a show
	 * @param typeOfShow  type of show. Possible values: TV show, movie, video.
	 * @param genre       Genre of show. Possible values are for example: comedy,
	 *                    drama etc.
	 * @param showTitle   name of show.
	 * @param realiseDate date of realise of show.
	 */
	public Show(Integer showId, String typeOfShow, String genre, String showTitle, String realiseDate) {
		super();
		this.showId = showId;
		this.typeOfShow = typeOfShow;
		this.genre = genre;
		this.showTitle = showTitle;
		this.realiseDate = realiseDate;
	}

	/**
	 * toString method to display Show object.
	 */
	@Override
	public String toString() {
		return "Show [showId=" + showId + ", typeOfShow=" + typeOfShow + ", genre=" + genre + ", showTitle=" + showTitle
				+ ", realiseDate=" + realiseDate + ", showLink=" + showLink + "]";
	}

	/**
	 * Deep object cloning Method used for cloning Show object.
	 * 
	 * @return cloned object of Show class
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Show clonedShow = (Show) super.clone();
		clonedShow.getShowLink().add((Link) clonedShow.getShowLink().get(0).clone());
		return clonedShow;
	}

	/**
	 * Method used for sorting shows based on their ID.
	 */
	public static Comparator<Show> ShowRatingComparator = new Comparator<Show>() {
		@Override
		public int compare(Show show1, Show show2) {
			return (int) (show2.getShowId() - show1.getShowId());
		}
	};

}

package com.app.beans;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.Entity;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * TvShow is a class that extends from Show, that it - it has attributes from
 * Show as well as its own.
 * 
 * @author Joanna Bieganowska
 */

@Entity
@NoArgsConstructor
public class TvShow extends Show implements Comparable<TvShow>, Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 28436372337597922L;

	/**
	 * ID of a TV show
	 */
	@Getter
	private Integer tvShowId;

	/**
	 * Average length of episode in minutes.
	 */
	@Getter
	@Setter
	private Integer averageEpisodeLength;

	/**
	 * Average number of episodes in season.
	 */
	@Transient
	@Getter
	@Setter
	private Integer averageNumOfEpisodesSeason;

	/**
	 * Number of TV show seasons.
	 */
	@Getter
	@Setter
	private Integer numberOfSeasons;

	/**
	 * Rating of the TV show, scale 0-10
	 */
	@Getter
	@Setter
	private Double tvShowRating;

	/**
	 * List of TV shows.
	 */
	@Transient
	public List<TvShow> tvShowList = new ArrayList<TvShow>();

	/**
	 * Parameterised constructor for creating TvShow object
	 * 
	 * @param showId               ID of the show
	 * @param typeOfShow           type of show. Possible values: TV show, movie,
	 *                             video.
	 * @param genre                Genre of show. Possible values are for example:
	 *                             comedy, drama etc.
	 * @param showTitle            name of show.
	 * @param realiseDate          realiseDate is date of realise of show. In case
	 *                             of video it is a date of video upload on Youtube.
	 *                             In case of movies it is a date of first global
	 *                             realise.
	 * @param tvShowId             ID of TV show
	 * @param averageEpisodeLength Average length of episode in minutes.
	 * @param numberOfSeasons      Number of TV show seasons.
	 * @param tvShowRating         Rating of the TV show, scale 0-10
	 */
	public TvShow(Integer showId, String typeOfShow, String genre, String showTitle,
			String realiseDate/* , Link showLink */, Integer tvShowId, Integer averageEpisodeLength,
			Integer numberOfSeasons, Double tvShowRating) {
		super(showId, typeOfShow, genre, showTitle, realiseDate/* , showLink */);
		this.tvShowId = tvShowId;
		this.averageEpisodeLength = averageEpisodeLength;
		this.numberOfSeasons = numberOfSeasons;
		this.tvShowRating = tvShowRating;
	}

	/**
	 * toString method to display TvShow object.
	 */
	@Override
	public String toString() {
		return "TvShow [tvShowId=" + tvShowId + "showID" + super.getShowTitle() + ", typeOfShow="
				+ super.getTypeOfShow() + ", genre=" + super.getGenre() + ", showTitle=" + super.getShowTitle()
				+ ", realiseDate=" + super.getRealiseDate() + ", showLink=" + super.getShowLink()
				+ ", averageEpisodeLength=" + averageEpisodeLength + ", numberOfSeasons=" + numberOfSeasons
				+ ", tvShowRating=" + tvShowRating + "]";
	}

	/**
	 * Method for calculating total length of series (value returned in hours).
	 * 
	 * @param tvShow object of TV show class
	 * @return lengthInHours length of the TV show in hours
	 */
	public Double totalSeriesLength(TvShow tvShow) {
		Double totalLength = Double
				.valueOf(tvShow.numberOfSeasons * tvShow.averageNumOfEpisodesSeason * tvShow.averageEpisodeLength);
		Double lengthInHours = totalLength / 60;
		return lengthInHours;
	}

	/**
	 * Clone method from Cloneable interface.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * Default sorting of TV show list based on id. Use of Comparable.
	 */
	@Override
	public int compareTo(TvShow tvShow) {
		return (this.tvShowId - tvShow.tvShowId);
	}

	/**
	 * Customised sorting of TV show list (by ratings). Use of Comprator.
	 */
	public static Comparator<TvShow> RatingComparator = new Comparator<TvShow>() {
		@Override
		public int compare(TvShow tvShow1, TvShow tvShow2) {
			return (int) ((tvShow2.getTvShowRating() - tvShow1.getTvShowRating()) * 10000);
		}
	};

	/**
	 * Overridden method implemented from runnable interface. It paralelly generated
	 * data with the use of threads.
	 */
	@Override
	public void run() {
		System.out.println("Generating TV shows...");
		Random random = new Random();

		/**
		 * Lists with possible values
		 */
		List<String> genre = Arrays.asList("comedy", "action", "drama", "horror", "thriller", "musical", "scifi");
		List<String> websites = Arrays.asList("Netflix", "YouTube", "cda", "Amazon", "CoolMovies", "MoviesAllTime");
		List<String> domains = Arrays.asList("com", "pl", "fr", "in", "uk", "org");

		/**
		 * Creating passed number of TV show objects.
		 */
		for (int i = 1; i <= numberOfObjects; i++) {

			/**
			 * Random index selection of above lists
			 */
			Integer genreInt = random.nextInt(6);
			Integer websiteInt = random.nextInt(6);
			Integer domainInt = random.nextInt(6);

			/**
			 * Generating random string
			 */
			int leftLimit = 65; // from A
			int rightLimit = 122; // to z
			int stringLength = 7;
			StringBuilder buffer = new StringBuilder(stringLength);
			for (int j = 0; j < stringLength; j++) {
				int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
				buffer.append((char) randomLimitedInt);
			}
			String generatedString = buffer.toString();

			/**
			 * Generating random date
			 */
			LocalDate from = LocalDate.of(2016, 1, 1);
			LocalDate to = LocalDate.of(2017, 1, 1);
			long days = from.until(to, ChronoUnit.DAYS);
			long randomDays = ThreadLocalRandom.current().nextLong(days + 1);
			LocalDate randomDate = from.plusDays(randomDays);
			String ranDate = randomDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			/**
			 * Generating more random values for TV show attributes
			 */
			Boolean secure = random.nextBoolean();
			Boolean valid = random.nextBoolean();
			Integer randomLength = random.nextInt(60);
			Integer randomNOSeasons = random.nextInt(20);
			Double ranRating = Math.round((0 + (random.nextDouble() * (10))) * 100.0) / 100.0;

			Link link = new Link(i + 2 * numberOfObjects, websites.get(websiteInt), domains.get(domainInt),
					generatedString, secure, valid);
			/**
			 * Creating TV show object
			 */
			TvShow tvShow = new TvShow(i + 2 * numberOfObjects, "tvshow", genre.get(genreInt), generatedString, ranDate,
					i, randomLength, randomNOSeasons, ranRating);

			tvShow.getShowLink().add(link);
			link.setShow(tvShow);

			updateList(tvShow);
		}
		System.out.println("TV shows data creation done.");
	}

	/**
	 * TASK 2 LAB3 updateList is used as critical section. Here, multiple threads
	 * may want to access the same data. The problem solved with synchronized block.
	 */
	private void updateList(TvShow tvShow) {
		synchronized (tvShowList) {
			tvShowList.add(tvShow);
		}
	}

}

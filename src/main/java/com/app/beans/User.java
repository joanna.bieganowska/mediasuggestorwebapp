package com.app.beans;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * User is a class that represents account of user, enabling 
 * him/her to log into webapplication.
 * 
 * @author Joanna Bieganowska
 */
public class User implements Serializable{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3827760733025554510L;

	/**
	 * ID of user.
	 */
	@Getter
	@Setter
	private Integer userId;

	/**
	 * userName - nickname of the user account
	 */
	@Getter
	@Setter
	private String userName;

	/**
	 * currentEmotion stores current emotion of the user. Whether user is happy, sad
	 * or bored.
	 */
	@Getter
	@Setter
	private String currentEmotion;

	/**
	 * Age of user, for future implementation.
	 */
	@Getter
	@Setter
	private Integer userAge;

	/**
	 * Preference of the user. .Whether user wants to watch movie, tv show or video
	 */
	@Getter
	@Setter
	private String preference;

	/**
	 * Set of watch later shows.
	 */
	@Getter
	@Setter
	private Set<Show> watchLaterList;

	/**
	 * Parameterised constructor for creating User object.
	 * 
	 * @param userId   user's Id (generated in application)
	 * @param userName nickname of the user account
	 * @param userAge  age of the user
	 */
	public User(Integer userId, String userName, Integer userAge) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userAge = userAge;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", currentEmotion=" + currentEmotion + ", userAge="
				+ userAge + ", preference=" + preference + ", watchLaterShows=" + watchLaterList + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}

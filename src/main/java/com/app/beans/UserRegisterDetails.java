package com.app.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;

@Named
@RequestScoped
public class UserRegisterDetails {
	
	@Getter
	@Setter
	private String input;
	
	@Getter
    private String output;

    public void submit() {
        output = "Hello World! You have typed: " + input;
    }
    
}

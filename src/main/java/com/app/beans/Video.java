package com.app.beans;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.Entity;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Video is a class that extends from Show, that is - it has attributes from
 * Show as well as its own.
 * 
 * @author Joanna Bieganowska
 */

@Entity
@NoArgsConstructor
public class Video extends Show implements Runnable {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5477432549939178484L;

	/**
	 * ID of video
	 */
	@Getter
	@Setter
	private Integer videoId;

	/**
	 * channelName is in other words the author of video, his channel name for
	 * example on YouTube
	 */
	@Getter
	@Setter
	private String channelName;

	/**
	 * Number of video likes (thumbs up).
	 */
	@Getter
	@Setter
	private Integer numberOfLikes;

	/**
	 * Number of dislikes (thumbs down)
	 */
	@Getter
	@Setter
	private Integer numberOfDislikes;

	/**
	 * List of videos.
	 */
	@Transient
	public List<Video> videoList = new ArrayList<Video>();

	/**
	 * videos is list of videos used by threads
	 */
	@Transient
	public List<Show> videos = new ArrayList<>();

	/**
	 * Parameterised constructor for creating Video object
	 * 
	 * @param showId           ID of the show
	 * @param typeOfShow       type of show. Possible values: TV show, movie, video.
	 * @param genre            Genre of show. Possible values are for example:
	 *                         comedy, drama etc.
	 * @param showTitle        name of show.
	 * @param realiseDate      realiseDate is date of realise of show. In case of
	 *                         video it is a date of video upload on Youtube. In
	 *                         case of movies it is a date of first global realise.
	 * @param videoId          video ID
	 * @param channelName      in other words the author of video, his channel name
	 *                         for example on YouTube
	 * @param numberOfLikes    Number of video likes (thumbs up).
	 * @param numberOfDislikes Number of dislikes (thumbs down)
	 */
	public Video(Integer showId, String typeOfShow, String genre, String showTitle, String realiseDate, /*
																										 * Link
																										 * showLink,
																										 */
			Integer videoId, String channelName, Integer numberOfLikes, Integer numberOfDislikes) {
		super(showId, typeOfShow, genre, showTitle, realiseDate/* , showLink */);
		this.videoId = videoId;
		this.channelName = channelName;
		this.numberOfLikes = numberOfLikes;
		this.numberOfDislikes = numberOfDislikes;
	}

	/**
	 * toString method to display Video object
	 */
	@Override
	public String toString() {
		return "Video [videoId=" + videoId + super.getShowTitle() + ", typeOfShow=" + super.getTypeOfShow() + ", genre="
				+ super.getGenre() + ", showTitle=" + super.getShowTitle() + ", realiseDate=" + super.getRealiseDate()
				+ ", showLink=" + super.getShowLink() + ", channelName=" + channelName + ", numberOfLikes="
				+ numberOfLikes + ", numberOfDislikes=" + numberOfDislikes + "]";
	}

	/**
	 * Overridden method implemented from runnable interface. Method that creates
	 * videos objects with the use of threads (paralelly).
	 */
	@Override
	public void run() {
		System.out.println("Generating videos...");
		Random random = new Random();

		/**
		 * Lists with possible values
		 */
		List<String> genre = Arrays.asList("comedy", "action", "drama", "horror", "thriller", "musical", "scifi");
		List<String> websites = Arrays.asList("Videolix", "YouTube", "cda", "AmazonVideos", "CoolVideos",
				"VideosAllTime");
		List<String> domains = Arrays.asList("com", "pl", "fr", "in", "uk", "org");
		List<String> channelName = Arrays.asList("PewDiePie", "MKBHD", "Naruciak", "Poszukiwacz", "Gonciarz",
				"Vines247");

		/**
		 * Creating passed number of video objects.
		 */
		for (int i = 1; i <= numberOfObjects; i++) {

			/**
			 * Random index selection of above lists
			 */
			Integer genreInt = random.nextInt(6);
			Integer websiteInt = random.nextInt(6);
			Integer domainInt = random.nextInt(6);
			Integer channelInt = random.nextInt(6);

			/**
			 * Generating random string
			 */
			int leftLimit = 65; // from A
			int rightLimit = 122; // to z
			int stringLength = 7;
			StringBuilder buffer = new StringBuilder(stringLength);
			for (int j = 0; j < stringLength; j++) {
				int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
				buffer.append((char) randomLimitedInt);
			}
			String generatedString = buffer.toString();

			/**
			 * Generating random date
			 */
			LocalDate from = LocalDate.of(2016, 1, 1);
			LocalDate to = LocalDate.of(2017, 1, 1);
			long days = from.until(to, ChronoUnit.DAYS);
			long randomDays = ThreadLocalRandom.current().nextLong(days + 1);
			LocalDate randomDate = from.plusDays(randomDays);
			String ranDate = randomDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			/**
			 * Generating more random values for Video attributes
			 */
			Boolean secure = random.nextBoolean();
			Boolean valid = random.nextBoolean();
			Integer randomLikes = random.nextInt(20000) + 1;
			Integer randomDislikes = random.nextInt(20000);

			Link link = new Link(i + numberOfObjects, websites.get(websiteInt), domains.get(domainInt), generatedString,
					secure, valid);

			/**
			 * Creating Video object with generated values
			 */
			Video video = new Video(i + numberOfObjects, "video", genre.get(genreInt), generatedString, ranDate, i,
					channelName.get(channelInt), randomLikes, randomDislikes);
			video.getShowLink().add(link);
			link.setShow(video);
		}
		System.out.println("Video data creation done.");
	}
}

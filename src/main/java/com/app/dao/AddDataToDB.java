package com.app.dao;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.app.beans.Link;
import com.app.beans.Movie;
import com.app.beans.Show;
import com.app.beans.TvShow;
import com.app.beans.Video;

/**
 * Class handles adding entities to embedded db.
 */
public class AddDataToDB {
	
	/**
	 * Main method for filling the database with data (one-time use only)
	 * @param args arguments for main
	 */
	public static void main(String[] args) {

		/**
		 * Configurations for connecting to database.
		 */
		Configuration config = new Configuration().configure().
										addAnnotatedClass(Show.class).
										addAnnotatedClass(Link.class).
										addAnnotatedClass(Movie.class).
										addAnnotatedClass(Video.class).
										addAnnotatedClass(TvShow.class);
        ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();     
        SessionFactory sf = config.buildSessionFactory(registry);   
        Session session = sf.openSession();
        
        session.beginTransaction();
        

	    Random random = new Random();

		/**
		 * Lists with possible values
		 */
		List<String> genre = Arrays.asList("comedy", "action", "drama", "horror", "thriller", "musical", "scifi");
		List<String> websites = Arrays.asList("Netflix", "YouTube", "cda", "Amazon", "CoolMovies", "MoviesAllTime");
		List<String> domains = Arrays.asList("com", "pl", "fr", "in", "uk", "org");
		List<String> producers = Arrays.asList("Netflix", "Warner", "Disney", "Pixar", "Paramount", "BestProducer");

		/**
		 * Creating passed number of movie objects.
		 */
		for (int i = 1; i <= Show.numberOfObjects; i++) {

			/**
			 * Random index selection of above lists
			 */
			Integer genreInt = random.nextInt(6);
			Integer websiteInt = random.nextInt(6);
			Integer domainInt = random.nextInt(6);
			Integer procuderInt = random.nextInt(6);

			/**
			 * Generating random string
			 */
			int leftLimit = 65; // from A
			int rightLimit = 122; // to z
			int stringLength = 7;
			StringBuilder buffer = new StringBuilder(stringLength);
			for (int j = 0; j < stringLength; j++) {
				int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
				buffer.append((char) randomLimitedInt);
			}
			String generatedString = buffer.toString();

			/**
			 * Generating random date
			 */
			LocalDate from = LocalDate.of(2010, 1, 1);
			LocalDate to = LocalDate.of(2020, 4, 4);
			long days = from.until(to, ChronoUnit.DAYS);
			long randomDays = ThreadLocalRandom.current().nextLong(days + 1);
			LocalDate randomDate = from.plusDays(randomDays);
			String ranDate = randomDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			/**
			 * Generating more random values for movie attributes
			 */
			Boolean secure = random.nextBoolean();
			Boolean valid = random.nextBoolean();
			Integer randomLength = random.nextInt(200);
			Double ranRating = Math.round((0 + (random.nextDouble() * (10))) * 100.0) / 100.0;
			
			Link link = new Link(i, websites.get(websiteInt), domains.get(domainInt), generatedString, secure, valid);
			Movie movie = new Movie(i, "movie", genre.get(genreInt), generatedString, ranDate, i, randomLength, producers.get(procuderInt), ranRating);
			movie.getShowLink().add(link);
			link.setShow(movie);
			
			session.save(movie);
			session.save(link);
		}
		
		//Random random = new Random();

		/**
		 * Lists with possible values
		 */
//		List<String> genre = Arrays.asList("comedy", "action", "drama", "horror", "thriller", "musical", "scifi");
//		List<String> websites = Arrays.asList("Videolix", "YouTube", "cda", "AmazonVideos", "CoolVideos", "VideosAllTime");
//		List<String> domains = Arrays.asList("com", "pl", "fr", "in", "uk", "org");
		List<String> channelName = Arrays.asList("PewDiePie", "MKBHD", "Naruciak", "Poszukiwacz", "Gonciarz", "Vines247");

		/**
		 * Creating passed number of video objects.
		 */
		for (int i = 1; i <= Show.numberOfObjects; i++) {

			/**
			 * Random index selection of above lists
			 */
			Integer genreInt = random.nextInt(6);
			Integer websiteInt = random.nextInt(6);
			Integer domainInt = random.nextInt(6);
			Integer channelInt = random.nextInt(6);

			/**
			 * Generating random string
			 */
			int leftLimit = 65; // from A
			int rightLimit = 122; // to z
			int stringLength = 7;
			StringBuilder buffer = new StringBuilder(stringLength);
			for (int j = 0; j < stringLength; j++) {
				int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
				buffer.append((char) randomLimitedInt);
			}
			String generatedString = buffer.toString();

			/**
			 * Generating random date
			 */
			LocalDate from = LocalDate.of(2016, 1, 1);
			LocalDate to = LocalDate.of(2017, 1, 1);
			long days = from.until(to, ChronoUnit.DAYS);
			long randomDays = ThreadLocalRandom.current().nextLong(days + 1);
			LocalDate randomDate = from.plusDays(randomDays);
			String ranDate = randomDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			/**
			 * Generating more random values for Video attributes
			 */
			Boolean secure = random.nextBoolean();
			Boolean valid = random.nextBoolean();
			Integer randomLikes = random.nextInt(20000) + 1;
			Integer randomDislikes = random.nextInt(20000);

			Link link = new Link(i + Show.numberOfObjects, websites.get(websiteInt), domains.get(domainInt), generatedString, secure, valid);
			
			/**
			 * Creating Video object with generated values
			 */
			Video video = new Video(i + Show.numberOfObjects, "video", genre.get(genreInt), generatedString, ranDate, i, channelName.get(channelInt), randomLikes, randomDislikes);
			video.getShowLink().add(link);
			link.setShow(video);
			
			session.save(video);
			session.save(link);
		}

		/**
		 * Creating passed number of TV show objects.
		 */
		for (int i = 1; i <= Show.numberOfObjects; i++) {

			/**
			 * Random index selection of above lists
			 */
			Integer genreInt = random.nextInt(6);
			Integer websiteInt = random.nextInt(6);
			Integer domainInt = random.nextInt(6);

			/**
			 * Generating random string
			 */
			int leftLimit = 65; // from A
			int rightLimit = 122; // to z
			int stringLength = 7;
			StringBuilder buffer = new StringBuilder(stringLength);
			for (int j = 0; j < stringLength; j++) {
				int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
				buffer.append((char) randomLimitedInt);
			}
			String generatedString = buffer.toString();

			/**
			 * Generating random date
			 */
			LocalDate from = LocalDate.of(2016, 1, 1);
			LocalDate to = LocalDate.of(2017, 1, 1);
			long days = from.until(to, ChronoUnit.DAYS);
			long randomDays = ThreadLocalRandom.current().nextLong(days + 1);
			LocalDate randomDate = from.plusDays(randomDays);
			String ranDate = randomDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			/**
			 * Generating more random values for TV show attributes
			 */
			Boolean secure = random.nextBoolean();
			Boolean valid = random.nextBoolean();
			Integer randomLength = random.nextInt(60);
			Integer randomNOSeasons = random.nextInt(20);
			Double ranRating = Math.round((0 + (random.nextDouble() * (10))) * 100.0) / 100.0;

			Link link = new Link(i + 2*Show.numberOfObjects, websites.get(websiteInt), domains.get(domainInt), generatedString, secure, valid);
			
			/**
			 * Creating TV show object
			 */
			TvShow tvShow = new TvShow(i + 2*Show.numberOfObjects, "tvshow", genre.get(genreInt), generatedString, ranDate, i, randomLength, randomNOSeasons, ranRating);
			
			tvShow.getShowLink().add(link);
			link.setShow(tvShow);

			session.save(tvShow);
			session.save(link);
		}
		
		session.getTransaction().commit();
		session.close();
		sf.close();
	}

	
}

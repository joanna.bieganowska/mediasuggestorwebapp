package com.app.dao;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;

import com.app.beans.Link;
import com.app.beans.Movie;
import com.app.beans.Show;
import com.app.beans.TvShow;
import com.app.beans.Video;

/**
 * FetchingData is a class that enables HQL queries for the project's purpose.
 * 
 * @author Joanna Bieganowska
 */

public class FetchingDataDao {

	/**
	 * Method for user emotion with show type comparison
	 * 
	 * @param mood       user's mood
	 * @param showChoice user's choice of the show
	 * @return matchedShows list of tailored results
	 */

	public List<Show> getPersonalizedShows(String mood, String showChoice) {
		String userPreference = showChoice.toLowerCase();
		String currentEmotion = mood.toLowerCase();

		Configuration config = new Configuration().configure().addAnnotatedClass(Show.class)
				.addAnnotatedClass(Link.class).addAnnotatedClass(Movie.class).addAnnotatedClass(Video.class)
				.addAnnotatedClass(TvShow.class);
		ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
		SessionFactory sf = config.buildSessionFactory(registry);
		Session session = sf.openSession();

		session.beginTransaction();

		List<Show> matchedShows = new ArrayList<>();
//		Transaction transaction = null;
		try /* (Session session = HibernateUtil.getSessionFactory().openSession()) */ {
			// start a transaction
//			transaction = session.beginTransaction();

			String genre1 = "";
			String genre2 = "";
			String genre3 = "";
			String query = "";
			if (currentEmotion.equals("happy")) {
				genre1 = "comedy";
				genre2 = "action";
				genre3 = "musical";
			} else if (currentEmotion.equals("sad")) {
				genre1 = "drama";
				genre2 = "horror";
				genre3 = "thriller";
			}

			if (currentEmotion.equals("bored")) {
				if (userPreference.equals("movie"))
					query = "from Movie";
				else if (userPreference.equals("video"))
					query = "from Video";
				else
					query = "from TvShow";
			} else {
				if (userPreference.equals("movie"))
					query = "from Movie s where s.genre in (:b, :c, :d)";
				else if (userPreference.equals("video"))
					query = "from Video s where s.genre in (:b, :c, :d)";
				else
					query = "from TvShow s where s.genre in (:b, :c, :d)";
			}

			Query q = session.createQuery(query);

			if (!currentEmotion.equals("bored")) {
				q.setParameter("b", genre1);
				q.setParameter("c", genre2);
				q.setParameter("d", genre3);
			}

			q.setMaxResults(10);

			matchedShows = (List<Show>) q.list();
//			session.getTransaction().commit();
			session.close();
			sf.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return matchedShows;

	}

	/**
	 * mostDislikedVideo is a class that retrieves from the database the most
	 * disliked video - that is the video with the biggest dislikes among all likes
	 * and dislikes
	 * 
	 * @return vidResult the most disliked video
	 */

	public Video getMostDislikedVideo() {
		Video vidResult = null;
		try {

			Configuration config = new Configuration().configure().addAnnotatedClass(Show.class)
					.addAnnotatedClass(Link.class).addAnnotatedClass(Movie.class).addAnnotatedClass(Video.class)
					.addAnnotatedClass(TvShow.class);
			ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties())
					.build();
			SessionFactory sf = config.buildSessionFactory(registry);
			Session session = sf.openSession();

			session.beginTransaction();

			// query for most disliked video
			String queryMDV = "from Video order by (numberofdislikes/double(numberofdislikes+numberoflikes))*100 desc";
			Query q = session.createQuery(queryMDV);
			q.setMaxResults(1);
			vidResult = (Video) q.uniqueResult();

			session.close();
			sf.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return vidResult;
	}

	/**
	 * Method for getting all videos of a particular channel that is passed in the
	 * parameter.
	 * 
	 * @param channelName name of the passed channel
	 * @return videosFromChannel list of channel's videos
	 */
	public List<Video> getAllVideosFromChannel(String channelName) {
		Configuration config = new Configuration().configure().addAnnotatedClass(Show.class)
				.addAnnotatedClass(Link.class).addAnnotatedClass(Movie.class).addAnnotatedClass(Video.class)
				.addAnnotatedClass(TvShow.class);
		ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
		SessionFactory sf = config.buildSessionFactory(registry);
		Session session = sf.openSession();

		session.beginTransaction();

		String queryString = "from Video where channelname = :name";
		Query q = session.createQuery(queryString);
		q.setParameter("name", channelName);
		List<Video> videosFromChannel = q.getResultList();

		// length 0 of result list indicates that query didn't find anything
		if (videosFromChannel.size() == 0) {
			System.out.println("Channel not found :(");
			return null;
		}

		session.close();
		sf.close();

		return videosFromChannel;
	}

	/**
	 * Method that deals with video object manipulation in terms of its number of
	 * likes, dislikes and existance in the database.
	 * 
	 * @param videoId ID of the video
	 * @param action  action action desired to be accomplished with the video
	 */
	public void updateVideoWithLikeOrDislike(Integer videoId, String action) {
		Configuration config = new Configuration().configure().addAnnotatedClass(Show.class)
				.addAnnotatedClass(Link.class).addAnnotatedClass(Movie.class).addAnnotatedClass(Video.class)
				.addAnnotatedClass(TvShow.class);
		ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
		SessionFactory sf = config.buildSessionFactory(registry);
		Session session = sf.openSession();

		session.beginTransaction();

		Video video = session.find(Video.class, videoId);
		if ("like".equals(action)) {
			video.setNumberOfLikes(video.getNumberOfLikes() + 1);
		} else if ("dislike".equals(action)) {
			video.setNumberOfDislikes(video.getNumberOfDislikes() + 1);
		} else if ("delete".equals(action)) {
			session.delete(video);
		}
		session.getTransaction().commit();
		session.close();
		sf.close();
	}

	/**
	 * Method that adds new video to the database.
	 * 
	 * @param channelName channel of the new entity
	 * @param videoTitle title of the new entity
	 * @param videoLink link of the new entity
	 * @param videoLikes number of likes of the new entity
	 */
	public void addNewVideo(String channelName, String videoTitle, String videoLink, Integer videoLikes,
			Integer videoDisLikes) {
		try {
			Configuration config = new Configuration().configure().addAnnotatedClass(Show.class)
					.addAnnotatedClass(Link.class).addAnnotatedClass(Movie.class).addAnnotatedClass(Video.class)
					.addAnnotatedClass(TvShow.class);
			ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties())
					.build();
			SessionFactory sf = config.buildSessionFactory(registry);
			Session session = sf.openSession();

			session.beginTransaction();

			Video lastVideoInTable = (Video) session.createQuery("from Video ORDER BY showId DESC").setMaxResults(1)
					.uniqueResult();

			Query query = session.createQuery("from Link order by lid DESC");
			query.setMaxResults(1);
			Link lastLink = (Link) query.uniqueResult();

			URL aURL = new URL(videoLink);

			String a1 = aURL.getHost();
			String[] websiteDomain = a1.split("\\.");
			String website = websiteDomain[0];

			String domain = websiteDomain[1];
			String extension = aURL.getPath();
			boolean secureProtocol = (aURL.getProtocol() == "https");
			boolean validity = true;

			Link link = new Link(lastLink.getLId() + 401, website, domain, extension, secureProtocol, validity);

			// filling unknown fields as simplification in testing stage of the application
			Video video = new Video(lastVideoInTable.getShowId() + 401, "video", lastVideoInTable.getGenre(), videoTitle,
					lastVideoInTable.getRealiseDate(), lastVideoInTable.getVideoId() + 1, channelName, videoLikes,
					videoDisLikes);
			video.getShowLink().add(link);
			link.setShow(video);
			
			session.save(video);
			session.save(link);

			session.getTransaction().commit();
			session.close();
			sf.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * getOneRandomMovieVideoTvShow is a method that retrieves randomly one movie, one tvShow and one video. 
	 * @return oneRandomMovieVideoTvShow
	 */
	public List<Show> getOneRandomMovieVideoTvShow(String[] typeOfShow) {
		
		List<Show> oneRandomMovieVideoTvShow = new ArrayList<>();
		boolean isMovie = false;
		boolean isTvShow= false;
		boolean isVideo = false;
		
		for(String type : typeOfShow) {
			if("Movie".equals(type)) isMovie = true;
			else if("TvShow".equals(type)) isTvShow = true;
			else isVideo = true;
		}
        try {
        	Configuration config = new Configuration().configure().addAnnotatedClass(Show.class)
					.addAnnotatedClass(Link.class).addAnnotatedClass(Movie.class).addAnnotatedClass(Video.class)
					.addAnnotatedClass(TvShow.class);
			ServiceRegistry registry = new StandardServiceRegistryBuilder().applySettings(config.getProperties())
					.build();
			SessionFactory sf = config.buildSessionFactory(registry);
			Session session = sf.openSession();

			session.beginTransaction();
        	// start a transaction
            int id = 1;
            String randomShowQuery = "";
            //query for most disliked video
            Random random = new Random();
            
            //random movie
            if(isMovie) {
            	id = random.nextInt(99) + 1;
                randomShowQuery = "from Movie where movieId = :id";
        		Query queryMovie = session.createQuery(randomShowQuery);
        		queryMovie.setParameter("id", id);
        		Movie randomMovies = (Movie) queryMovie.uniqueResult();
        		oneRandomMovieVideoTvShow.add(randomMovies);
            }
            
            //random tvShow
            if(isTvShow) {
            	id = random.nextInt(99) + 1;
                randomShowQuery = "from TvShow where tvShowId = :id";
                Query queryTvShow = session.createQuery(randomShowQuery);
                queryTvShow.setParameter("id", id);
        		TvShow randomTvShow = (TvShow) queryTvShow.uniqueResult();
        		oneRandomMovieVideoTvShow.add(randomTvShow);
            }
            
            //random Video
            if(isVideo) {
            	id = random.nextInt(99) + 1;
                randomShowQuery = "from Video where videoId = :id";
                Query queryVideo = session.createQuery(randomShowQuery);
                queryVideo.setParameter("id", id);
        		Video randomVideo = (Video) queryVideo.uniqueResult();
        		oneRandomMovieVideoTvShow.add(randomVideo);
            }
    		
    		session.getTransaction().commit();
        }  catch (Exception e) {
			e.printStackTrace();
		}
        return oneRandomMovieVideoTvShow;
	}

}

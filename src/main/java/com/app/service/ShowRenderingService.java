package com.app.service;

import java.util.List;

import com.app.beans.Show;
import com.app.beans.Video;
import com.app.dao.FetchingDataDao;

/**
 * ShowRenderingService is a class that deals with business logic and appropriately redirects to the database methods. 
 * @author Joanna Bieganowska
 *
 */
public class ShowRenderingService {
	
	private FetchingDataDao fetchShowData = new FetchingDataDao();
	
	/**
	 * Method that redirects to method which gets personalised shows for the user.
	 * @param mood user's mood 
	 * @param showChoice user's choice of show type
	 * @return personaLizedList list of shows that are personalised for the user
	 */
	public List<Show> showBasedOnMoodService(String mood, String showChoice) {
		List<Show> personaLizedList = fetchShowData.getPersonalizedShows(mood, showChoice);
		return personaLizedList;
	}
	
	/**
	 * Method that redirects to the method that gets most disliked video in the database.
	 * @return mostDislikedVideo video with biggest percentage of dislikes
	 */
	public Video mostDislikedVideoService() {
		Video mostDislikedVideo = fetchShowData.getMostDislikedVideo();
		return mostDislikedVideo;
	}

	/**
	 * Method that redirects to the method which obtains videos of a particular channel
	 * @param channelName name of the channel
	 * @return videosFromChannel list of channel's videos
	 */
	public List<Video> VideosFromChannel(String channelName) {
		List<Video> videosFromChannel = fetchShowData.getAllVideosFromChannel(channelName);
		return videosFromChannel;
	}

	/**
	 * Method that redirects to the method that updates the parameter of the video object with regard to given action
	 * @param videoId ID of the video object
	 * @param action action that indicates what should be done with video object
	 */
	public void updateVideo(Integer videoId, String action) {
		fetchShowData.updateVideoWithLikeOrDislike(videoId, action);
	}

	
	//Add a new video to the database
	public void addNewVideoService(String channelName, String videoTitle, String videoLink, Integer videoLikes, Integer videoDisLikes) {
		fetchShowData.addNewVideo(channelName, videoTitle, videoLink, videoLikes, videoDisLikes);
	}
	
	public List<Show> getOneRandomMovieVideoTvShow(String[] typeOfShow){
		List<Show> oneRandomMovieVideoTvShow = fetchShowData.getOneRandomMovieVideoTvShow(typeOfShow); 
		return oneRandomMovieVideoTvShow;
	}

}

package com.app.service;

/**
 * Class that velidates the user input in the login page. 
 * @author Joanna Bieganowska
 *
 */
public class UserValidationService {
	
	/**
	 * Method that checks whether login and password of the user are correct
	 * @param name user name
	 * @param password password of the user
	 * @return true if correct, false if incorrect
	 */
	public boolean isUserValid(String name, String password) {
		if(name.equals("Student") && password.equals("qwerty")) {
			return true;
		}
		return false;
	}
}

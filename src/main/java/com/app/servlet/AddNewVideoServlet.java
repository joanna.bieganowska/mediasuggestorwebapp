package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.service.ShowRenderingService;

/**
 * AddNewVideoServlet is a class that handles adding new content based on user data.
 * 
 * @author Joanna Bieganowska
 */
@WebServlet(urlPatterns = "/addNewVideo")
public class AddNewVideoServlet extends HttpServlet {
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = 8312296198581306167L;
	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";
	 /**
     * ShowRenderingService
     */
	private ShowRenderingService showRenderingService = new ShowRenderingService();

	// Get method for redirecting incoming request to addNewVideo.jsp
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(viewPath + "/addNewVideo.jsp").forward(req, resp);
	}

	// Post method which takes in video data entered by the user and send it to
	// appropriate service. Then it redirects to VideoAddedSuccessfully.jsp
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String videoTitle = req.getParameter("videoTitle");
		String videoLink = req.getParameter("videoLink");
		Integer videoLikes = Integer.valueOf(req.getParameter("videoLikes"));
		Integer videoDisLikes = Integer.valueOf(req.getParameter("videoDisLikes"));
		String channelName = (String) req.getSession().getAttribute("channelName");
		showRenderingService.addNewVideoService(channelName, videoTitle, videoLink, videoLikes, videoDisLikes);
		req.getRequestDispatcher(viewPath + "/VideoAddedSuccessfully.jsp").forward(req, resp);
	}

}

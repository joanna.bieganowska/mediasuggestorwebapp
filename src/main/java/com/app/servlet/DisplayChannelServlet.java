package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * DisplayChannelServlet is a class that redirects the request to the displayChannel page.
 * @author Joanna Bieganowska
 */
@WebServlet(urlPatterns = "/displayChannels")
public class DisplayChannelServlet extends HttpServlet {
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = 6380839361139135493L;
	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";

	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(viewPath + "/displayChannels.jsp").forward(req, resp);
	}
	
}

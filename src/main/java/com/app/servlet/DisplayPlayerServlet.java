package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * DisplayPlayerServlet is a class that redirects the request to the displayChannel page.
 * @author Joanna Bieganowska
 */
@WebServlet(urlPatterns = "/displayPlayer")
public class DisplayPlayerServlet extends HttpServlet {
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = 1L;   
	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";

	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(viewPath + "/displayPlayer.jsp").forward(req, resp);
	}
}

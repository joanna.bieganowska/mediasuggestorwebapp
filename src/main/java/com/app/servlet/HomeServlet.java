package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * HomeServlet is a class that redirects the request to the home page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/home")
public class HomeServlet extends HttpServlet {
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = -5211417313655936533L;

	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";

	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(viewPath + "/home.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}

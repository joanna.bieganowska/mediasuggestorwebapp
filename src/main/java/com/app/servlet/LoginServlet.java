package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.service.UserValidationService;

/**
 * LoginServlet is a class that redirects the request to the login page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet{
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = 7790853303670104133L;
	
	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";
	 /**
     * userValidationService
     */
	private UserValidationService userValidationService = new UserValidationService();
	
	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher(viewPath + "/login.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//get the details from the jsp eg form details
		String name = req.getParameter("name");
		String password = req.getParameter("password");
		
		boolean isValidUser = userValidationService.isUserValid(name, password);
		
		if(isValidUser) {
			req.getSession().setAttribute("name", name);
			req.getRequestDispatcher("/home").forward(req, resp);
		}else {
			req.setAttribute("error", "Invalid username or password");
			req.getRequestDispatcher(viewPath + "/login.jsp").forward(req, resp);
		}
		
	}
}

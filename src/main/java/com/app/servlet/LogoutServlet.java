package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * LogoutServlet is a class that redirects the request to the login page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/logout")
public class LogoutServlet extends HttpServlet{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7791882037882081001L;

	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";
	
	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().invalidate();
		req.getRequestDispatcher(viewPath + "/login.jsp").forward(req, resp);
	}
	
}

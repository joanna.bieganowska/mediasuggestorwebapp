package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.service.ShowRenderingService;

/**
 * MostDisLikedVideoServlet is a class that redirects the request to the mostDislikedVideo page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/mostDislikedVideo")
public class MostDisLikedVideoServlet extends HttpServlet{
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = 1026024142760115278L;
	 
	/**
     * ShowRenderingService
     */
	private ShowRenderingService showRenderingService = new ShowRenderingService();
	
	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";
	
	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("mostDisLikedVideo", showRenderingService.mostDislikedVideoService());
		req.getRequestDispatcher(viewPath + "/mostDislikedVideo.jsp").forward(req, resp);
	}
	
}

package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ShowBasedOnMoodServlet is a class that redirects the request to the showPersonalizedList page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/showBasedOnMood")
public class ShowBasedOnMoodServlet extends HttpServlet {
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = -47295153819109242L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("In Servlet");
		req.getRequestDispatcher("/showPersonalizedList").forward(req, resp);
	}

}

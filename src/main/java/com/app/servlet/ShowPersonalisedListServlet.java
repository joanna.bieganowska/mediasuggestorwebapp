package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.service.ShowRenderingService;

/**
 * ShowPersonalisedListServlet is a class that redirects the request to the showPersonalizedList page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/showPersonalizedList")
public class ShowPersonalisedListServlet extends HttpServlet{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7480702137356625439L;
	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";
	 /**
     * ShowRenderingService
     */
	private ShowRenderingService showBasedOnMoodService = new ShowRenderingService();
	
	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		req.setAttribute("todos", toDoService.getAllToDos());
		String mood = req.getParameter("mood");
		String showChoice = req.getParameter("showChoice");
		req.setAttribute("moodBasedShows", showBasedOnMoodService.showBasedOnMoodService(mood, showChoice));
		req.getRequestDispatcher(viewPath + "/showPersonalizedList.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}

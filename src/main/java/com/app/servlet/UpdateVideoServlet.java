package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.service.ShowRenderingService;

/**
 * UpdateVideoServlet is a class that redirects the request to the displayUpdatedVideos page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/updateVideo")
public class UpdateVideoServlet extends HttpServlet {
	 /**
     * UID of servlet object.
     */
	private static final long serialVersionUID = -4968586267714030802L;
	 /**
     * ShowRenderingService
     */
	private ShowRenderingService showRenderingService = new ShowRenderingService();

	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("action");
		String videoId = req.getParameter("videoId");
		showRenderingService.updateVideo(Integer.valueOf(videoId), action);
		resp.sendRedirect("/MediaSuggestorWebApp/displayUpdatedVideos");
	}
	
}

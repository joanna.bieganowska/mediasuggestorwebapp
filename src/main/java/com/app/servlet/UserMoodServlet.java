package com.app.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * UserMoodServlet is a class that redirects the request to the userMood page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/userMood")
public class UserMoodServlet extends HttpServlet {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6006031759630682365L;
	 /**
     * ShowRenderingService
     */
	private static final String viewPath = "/WEB-INF/views";
	
	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//		req.setAttribute("todos", toDoService.getAllToDos());
		req.getRequestDispatcher(viewPath + "/userMood.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}

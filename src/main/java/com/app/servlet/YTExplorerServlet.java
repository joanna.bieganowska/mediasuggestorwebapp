package com.app.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.app.beans.Video;
import com.app.service.ShowRenderingService;

/**
 * YTExplorerServlet is a class that redirects the request to the displayVideosFromChannels page.
 * @author Joanna Bieganowska
 *
 */
@WebServlet(urlPatterns = "/ytExplorer")
public class YTExplorerServlet extends HttpServlet{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6412854481850086126L;
	 /**
     * ShowRenderingService
     */	
	private ShowRenderingService showRenderingService = new ShowRenderingService();
	 /**
     * Path to loaction of .jsp files
     */
	private static final String viewPath = "/WEB-INF/views";
	
	/**
	 * Method that gets request to display .jsp
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String channelName = req.getParameter("channelName");
		req.getSession().setAttribute("channelName", channelName);
		List<Video> videosFromChannel = showRenderingService.VideosFromChannel(channelName);
		req.setAttribute("videosFromChannel", videosFromChannel);
		req.getRequestDispatcher(viewPath + "/displayVideosFromChannels.jsp").forward(req, resp);
	}

}

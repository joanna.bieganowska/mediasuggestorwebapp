<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>

<div class="container">
	<h1>Video Addition Success Page</h1>
	<h2>Your video has been successfully added to the database. \n Please explore our other interesting options buy clicking Home button from the top</h2>
</div>

<%@ include file="../common/footer.jspf"%>

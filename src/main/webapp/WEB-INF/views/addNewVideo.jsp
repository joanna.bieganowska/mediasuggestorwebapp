<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>

<div class="container">
	<h1>Add New Video Page</h1>
	<form method="post" action="/MediaSuggestorWebApp/addNewVideo">
		<div class="form-group">
			<label>Video Title</label> <input type="text" class="form-control" name="videoTitle">
		</div>
		<div class="form-group">
			<label>Video Link</label> <input type="url" class="form-control" name="videoLink">
		</div>
		<div class="form-group">
			<label>Video Likes</label> <input type="number" class="form-control" name="videoLikes">
		</div>
		<div class="form-group">
			<label>Video DisLikes</label> <input type="number" class="form-control" name="videoDisLikes">
		</div>
		<button type="submit" class="btn btn-primary">Add</button>
	</form>
</div>

<%@ include file="../common/footer.jspf"%>

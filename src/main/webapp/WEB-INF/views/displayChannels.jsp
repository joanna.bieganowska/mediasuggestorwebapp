<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>
<div class="container">
	<h1>Home Page</h1>

	<table class="table table-hover table-striped table-dark">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Select a channels from from which you want to Like, Dislike or Delete a video</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">1</th>
				<td><a href="/MediaSuggestorWebApp/ytExplorer?channelName=Vines247">Vines247</a></td>
			</tr>
			<tr>
				<th scope="row">2</th>
				<td><a href="/MediaSuggestorWebApp/ytExplorer?channelName=MKBHD">MKBHD</a></td>
			</tr>
			<tr>
				<th scope="row">3</th>
				<td><a href="/MediaSuggestorWebApp/ytExplorer?channelName=PewDiePie">PewDiePie</a></td>
			</tr>
			<tr>
				<th scope="row">4</th>
				<td><a href="/MediaSuggestorWebApp/ytExplorer?channelName=Naruciak">Naruciak</a></td>
			</tr>
			<tr>
				<th scope="row">5</th>
				<td><a href="/MediaSuggestorWebApp/ytExplorer?channelName=Gonciarz">Gonciarz</a></td>
			</tr>
		</tbody>
	</table>
</div>

<%@ include file="../common/footer.jspf"%>
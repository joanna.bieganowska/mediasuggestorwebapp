<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>

<link href=”//vjs.zencdn.net/7.0/video-js.min.css” rel=”stylesheet”>
<script src=”//vjs.zencdn.net/7.0/video.min.js”></script>

<div class="container">
	<h1>Player</h1>
	<p>If you want to check out video about fractals, here it is:)</p>
	<br>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/RU0wScIj36o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<%@ include file="../common/footer.jspf"%>
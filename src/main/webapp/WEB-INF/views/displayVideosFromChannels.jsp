<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>
<script src="webjars/jquery/3.4.1/jquery.min.js"></script>
<script src="webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<h1>Show list Page</h1>

<div class="container">
	<H1>Welcome ${name}</H1>
	<div class="container" align="right" style="padding-top: 10px;padding-bottom: 10px;">
	<a class="btn btn-success"
							href="/MediaSuggestorWebApp/addNewVideo"
							role="button">Add New Video</a>
							</div>

	<div class="table-responsive">
		<table class="table table-hover table-striped table-dark">
			<caption>The videos from the channel are as above:</caption>
			<thead>
				<th>ShowId</th>
				<th>Show Title</th>
				<th>Realise Date</th>
				<th>Likes</th>
				<th>Dislikes</th>
				<th>Link</th>
				<th>Action</th>
			</thead>

			<tbody>
				<c:forEach items="${videosFromChannel}" var="video">
					<tr>
						<td>${video.showId}</td>
						<td>${video.showTitle}</td>
						<td>${video.realiseDate}</td>
						<td>${video.numberOfLikes}</td>
						<td>${video.numberOfDislikes}</td>
						<c:forEach items="${video.showLink}" var="link">
							<td>${link}</td>
						</c:forEach>
						<td><a class="btn btn-success"
							href="/MediaSuggestorWebApp/updateVideo?action=like&videoId=${video.showId}"
							role="button">Like</a></td>
						<td><a class="btn btn-warning"
							href="/MediaSuggestorWebApp/updateVideo?action=dislike&videoId=${video.showId}"
							role="button">Dislike</a></td>
						<td><a class="btn btn-danger"
							href="/MediaSuggestorWebApp/updateVideo?action=delete&videoId=${video.showId}"
							role="button">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$('#selector button')
			.click(
					function() {
						$(this).addClass('active').siblings().removeClass(
								'active');

						// TODO: insert whatever you want to do with $(this) here
						location.href = (location.href)
								.substr(
										0,
										(location.href)
												.lastIndexOf('displayVideosFromChannels.jsp'))
								+ "/MediaSuggestorWebApp/updateVideo?action=like&videoId=${video.videoId}";
					});
</script>
<%@ include file="../common/footer.jspf"%>

<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>
<div class="container">
	<h1>Home Page</h1>

	<table class="table table-hover table-striped table-dark">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Options for you</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">1</th>
				<td><a href="/MediaSuggestorWebApp/userMood.xhtml">Show
						personalized watch later list</a></td>
			</tr>
			<tr>
				<th scope="row">2</th>
				<td><a href="/MediaSuggestorWebApp/mostDislikedVideo">Show
						most disliked video</a></td>
			</tr>
			<tr>
				<th scope="row">3</th>
				<td><a href="/MediaSuggestorWebApp/displayChannels">YT
						Explorer (Like, Dislike or Delete the video if inappropriate)</a></td>
			</tr>
			<tr>
				<th scope="row">4</th>
				<td><a href="/MediaSuggestorWebApp/displayPlayer">Player</a></td>
			</tr>
			<tr>
				<th scope="row">5</th>
				<td><a href="/MediaSuggestorWebApp/selectRandomShow.xhtml">Select Random Show</a></td>
			</tr>
		</tbody>
	</table>
</div>

<%@ include file="../common/footer.jspf"%>

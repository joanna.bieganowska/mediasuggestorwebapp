<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>

<div class="container">
	<h1>Login Page</h1>
	<p>
		<font color="red">${error }</font>
	</p>
	<form method="post" action="/MediaSuggestorWebApp/login">
		<div class="form-group">
			<label>Name</label> <input type="text" class="form-control" name="name">
		</div>
		<div class="form-group">
			<label>Password</label> <input type="password" class="form-control" name="password">
		</div>
		<button type="submit" class="btn btn-primary">Login</button>
	</form>
</div>

<%@ include file="../common/footer.jspf"%>

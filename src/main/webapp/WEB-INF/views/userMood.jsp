<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>
<div class="container">
	<h1>User Mood Page</h1>

<div class="container" align="center">
	<form method="post" action="/MediaSuggestorWebApp/showBasedOnMood">
		<div class="form-group">
			<label>How are you feeling today?</label> <input type="text" class="form-control"
				name="mood" placeholder="Happy, Sad, Bored">
		</div>
		<div class="form-group">
			<label>What would you like to watch?</label> <input type="text" class="form-control"
				name="showChoice" placeholder="Movie, TV Show, Video">
		</div>

		<button type="submit" class="btn btn-success">Submit</button>
	</form>
	</div>
</div>

<%@ include file="../common/footer.jspf"%>

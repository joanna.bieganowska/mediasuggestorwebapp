SELECT * FROM sys.systables where tabletype = 'T'

drop table link;
drop table movie;
drop table tvshow;
drop table video;

select count(*) from link;

select * from link;
select * from movie;
select * from video;
select * from tvshow;

percent = Double.valueOf((video.getNumberOfDislikes())) / (video.getNumberOfDislikes() + video.getNumberOfLikes()) * 100;

select * from TVSHOW where tvshowid=1000;

-- 3 - most disliked video
select *
   from video
   order by (numberofdislikes/double(numberofdislikes+numberoflikes))*100 desc
   fetch first 1 rows only;

   
-- 2 - display personalized list of shows

-- HAPPY
select a.* from (
	select * from movie where genre in ('comedy', 'action', 'musical') fetch first 10 rows only) a
	order by a.movierating desc;

select a.* from (
	select * from video where genre in ('comedy', 'action', 'musical') fetch first 10 rows only) a
	order by (a.numberoflikes/double(a.numberofdislikes+a.numberoflikes)) desc;
	
select a.* from (
	select * from tvshow where genre in ('drama', 'horror', 'thriller') fetch first 10 rows only) a
	order by a.tvshowrating desc;
   
	
-- SAD
select a.* from (
	select * from movie where genre in ('drama', 'horror', 'thriller') fetch first 10 rows only) a
	order by a.movierating desc;

select a.* from (
	select * from video where genre in ('drama', 'horror', 'thriller') fetch first 10 rows only) a
	order by (a.numberoflikes/double(a.numberofdislikes+a.numberoflikes)) desc;
	
select a.* from (
	select * from tvshow where genre in ('comedy', 'action', 'musical') fetch first 10 rows only) a
	order by a.tvshowrating desc; 
 
	
-- BORED  
select * from movie fetch first 10 rows only;
select * from video fetch first 10 rows only;
select * from tvshow fetch first 10 rows only;

<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>
<h1>Show list Page</h1>

<div class="container">
	<H1>Welcome ${name}</H1>

	<table class="table table-hover table-striped table-dark">
		<caption>The Most Disliked video is:</caption>
		<thead>
			<th>ShowId</th>
			<th>Show Title</th>
			<th>Realise Date</th>
			<th>Dislikes</th>
			<th>Link</th>
		</thead>

		<tbody>
			<tr>
				<td>${mostDisLikedVideo.showId}</td>
				<td>${mostDisLikedVideo.showTitle}</td>
				<td>${mostDisLikedVideo.realiseDate}</td>
				<td>${mostDisLikedVideo.numberOfDislikes}</td>
				<c:forEach items="${mostDisLikedVideo.showLink}" var="link">
					<td>${link}</td>
				</c:forEach>
			</tr>
		</tbody>
	</table>
</div>

<%@ include file="../common/footer.jspf"%>

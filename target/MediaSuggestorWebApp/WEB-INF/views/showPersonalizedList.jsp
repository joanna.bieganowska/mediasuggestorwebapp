<%@ include file="../common/header.jspf"%>
<%@ include file="../common/navbar.jspf"%>
<h1>Show list Page</h1>

<div class="container">
	<H1>Welcome ${name}</H1>

	<table class="table table-hover table-striped table-dark">
		<caption>Here are you shows based on your mood ${mood} :</caption>
		<thead>
			<th>ShowId</th>
			<th>Show Title</th>
			<th>Realise Date</th>
			<th>Link</th>
		</thead>

		<tbody>
			<c:forEach items="${moodBasedShows}" var="show">
				<tr>
					<td>${show.showId}</td>
					<td>${show.showTitle}</td>
					<td>${show.realiseDate}</td>
					<c:forEach items="${show.showLink}" var="link">
						<td>${link}</td>
					</c:forEach>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<%@ include file="../common/footer.jspf"%>
